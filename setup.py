import setuptools

setuptools.setup(
    name="file_convert",
    version="0.0.3",
    author="Rui Meira",
    author_email="ruimiguelcm96@gmail.com",
    description="Package to convert files between various formats: TIFF, JPEG200, PDF",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)